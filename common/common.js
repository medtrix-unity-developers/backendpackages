﻿
var isiPadWebApp = false;

var standalone = window.navigator.standalone;
var userAgent = window.navigator.userAgent.toLowerCase();
var safari = /safari/.test(userAgent);
var ios = navigator.userAgent.match(/iPad/i);

if (ios) {
    if (!standalone && safari) {
        isiPadWebApp = false;
    } else if (standalone && !safari) {
        isiPadWebApp = false;
    } else if (!standalone && !safari) {
        isiPadWebApp = true;
    }
}

function isMobileBrowser() {
    if (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ) {
        return true;
    }
    else {
        return false;
    }
}

function SetDeviceToken(token) {

}

function onNetworkStatus(status) {
    if (parseInt(status) == 1) {
        isOnline = true;
    }
    else {
        isOnline = false;
    }
}

/* iOS header fix */
if ('ontouchstart' in window) {
    var $body = $('body');
    $(document)
    .on('focus', 'input', function () {
        $body.find('nav').addClass('fixfixed');
    })
    .on('blur', 'input', function () {
        $body.find('nav').removeClass('fixfixed');
    });
}

$(document).ready(function () {
    if ((isMobileBrowser()) && (navigator.userAgent.match(/iPad/i)) && (!isiPadWebApp)) {
       // window.location = "https://itunes.apple.com/us/app/equip-cme/id1108327867?mt=8";
    }
    $('body').attr('oncontextmenu', 'return false');
    var div = document.createElement("div");
    div.innerHTML = "<!--[if lt IE 10]><i></i><![endif]-->";
    var isIeLessThan10 = (div.getElementsByTagName("i").length == 1);
    if (isIeLessThan10) {
        alert("This site is best viewed in Internet Explorer 10 and above");
        return;
    }

    $('input[type="password"]').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".right-arrow").find("img").attr("src", "assets/images/down-arrow.png");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".right-arrow").find("img").attr("src", "assets/images/right-arrow.png");
    });

    $('#page-score').find('*').css('-webkit-transform', 'none');
    $('.modal').find('.modal-header').find('*').css('-webkit-transform', 'none');

    setWindowHeight();
    GetContactCategories();
	var requestedPage = sessionStorage.getItem('equip_request_page')
	if(requestedPage == null)
	{
		onLoadPopLogin();
	}
});

window.onresize = function () {
    setWindowHeight();
}

function setWindowHeight() {
    var height = $(window).height();
    $('#blockContainer').css('height', (height - 50) + 'px');
    $('.pt-page').find('.first-child').css('min-height', height + 'px');
    $('.pt-page').find('.right-bar-container').css('min-height', height + 'px');
    $('.tab-content').css('height', (height - 260) + 'px');

    var docHeight = parseInt($(document).height());

    if (docHeight < 400) {
        var menuWidth = parseInt($('.menu-item').width());
        var menuHeight = menuWidth - 90;
        $('.menu-item').css('height', menuHeight + 'px');
    }

    else {
        var menuHeight = parseInt($(document).height() / 2);
        $('.menu-item').css('height', (menuHeight - 25) + 'px');
    }
}

function showFooter() {
    window.setTimeout(function () {
        var footer = $('#myFooter').html();
        $('.customFooter').html(footer);
    }, 3000)
}

function GetContactCategories() {
    var result = lmsUser.GetContactCategories(new Contact()).result;
    if (result != null && result.length > 0) {
        var options = '<option value="">Select Category</option>';
        for (var index = 0; index < result.length; index++) {
            options += '<option value="' + result[index].categoryid + '">' + result[index].categoryname + '</option>';
        }

        $('#contactCategory').html(options);
    }
}

function submitQuery() {
    $('#contactMessage').hide();
    $('#contactMessage').css('color', '#D8000C');
    var data = new Contact();
    data.email = $.trim($('#queryForm').find('#userMail').val());
    data.subject = $.trim($('#queryForm').find('#contactSubject').val());
    data.category = $('#contactCategory').children("option").filter(":selected").val();
    data.description = $('#queryForm').find('#contactDescription').val();

    if (!document.getElementById('queryForm').checkValidity()) {
        if ((data.email).length == 0) {
            $('#contactMessage').html('Email is required');
            $('#queryForm').find('#userMail').focus();
        }

        else if (!isValidEmail(data.email)) {
            $('#contactMessage').html('Invalid Email address');
            $('#queryForm').find('#userMail').focus();
        }

        else if ((data.subject).length == 0) {
            $('#contactMessage').html('Subject is required');
            $('#queryForm').find('#contactSubject').focus();
        }

        else if (data.category == '') {
            $('#contactMessage').html('Please select a category');
        }

        else if ((data.description).length == 0) {
            $('#contactMessage').html('Please provide a description');
        }

        $('#contactMessage').show();

        return false;
    }

    $('#contactMessage').css('color', '#4F8A10');
    $('#contactMessage').html('Please wait');
    $('#contactMessage').show();

    $('#queryForm').find('button').hide();

    window.setTimeout(function () {
        if (lmsUser.AddContactQuery(data, null).result) {
            if (!($('#queryForm').find('#userMail').prop('readonly'))) {
                $('#queryForm').find('#userMail').val('');
            }
            $('#queryForm').find('#contactSubject').val('');
            $('#contactCategory option[value=""]').prop('selected', true);
            $('#queryForm').find('#contactDescription').val('');

            $('#contactMessage').css('color', '#4F8A10');
            $('#contactMessage').html('Your message has been submitted successfully');
        }
        else {
            $('#contactMessage').html('Sorry! Something went wrong');
        }

        $('#queryForm').find('button').show();
        $('#contactMessage').show();

        window.setTimeout(function () {
            $('#contactMessage').hide();
        }, 5000);

    }, 1000);
}

function showContactPage() {
    changePage('page-contact');
}

function showFAQPage() {
    changePage('page-faq');
}

function changePage(elementId) {
    var id = $('.pt-page-current').attr('id');
    $('#' + id).removeClass('pt-page-current');
    $('#' + elementId).addClass('pt-page-current');
}

function showFAQTab(of) {
    if (of == 'technical') {
        $('#module-tab').hide();
        $('#technical-tab').show();
        $('#module-tab-title').css('color', '#000000');
        $('#technical-tab-title').css('color', '#d52b1e');
    }

    else if (of == 'module') {
        $('#technical-tab').hide();
        $('#module-tab').show();
        $('#technical-tab-title').css('color', '#000000');
        $('#module-tab-title').css('color', '#d52b1e');
    }
}

function createAlert(message) {
    if (isMobileBrowser()) {
        $('#alert-modal').find('.modal-body').html(message);
        $('#alert-modal').modal('show');
    }
}


function onLoadPopLogin() {
    var isLoginPopMod = parseInt(localStorage.getItem("logingPop"));
    var isLoginFirstTime = parseInt(localStorage.getItem("firstTimeLogin"));
    if (isLoginFirstTime == 1 && isLoginPopMod == 0) {
        window.setTimeout(function () {
        $('#isLoginPopModal').modal('show');
          }, 1000);
        localStorage.setItem('logingPop', "1");
    }
}


function onClickPdf() {
    window.open('assets/pdf/Transcript.pdf', '_blank');
}

function getQueryParams(query) {
    query = query.split('+').join(' ');
    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;
    while (tokens = re.exec(query)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
}

function onClickOpenannouncmentPopModal() {
    var q = new Date();
    var m = q.getMonth();
    var d = q.getDate();
    var y = q.getFullYear();
    var date = new Date(y, m, d);
    mydate = new Date(2016, 9, 29);
    if (date < mydate) {
        $('#announcmentPopModal').modal('show');
    }
    else
    {
        return;
    }
}