﻿
// Omnificence - 2015

function validateEmail(link) {
    var valid = isValidEmail(link.value);
    if (!valid)
        createAlert('InvalidEMail');
    return valid;
}

function validateText(link) {
    var valid = isValidAlpha(link.value);
    if (!valid)
        createAlert('InvalidAlphabet');
    return valid;
}

function validateNumber(link) {
    var valid = isValidNumber(link.value);
    if (!valid)
        createAlert('InvalidNumber');
    return valid;
}

function isValidEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
}

function isValidAlpha(text) {
    var expr = /^[a-zA-Z ]*$/;
    return expr.test(text);
}

function isValidNumber(number) {
    var expr = /^[0-9]+$/;
    return expr.test(number);
}

function isValidTime(time) {
    var expr = /^(0[1-9]|1[012]):([0-5]\d)\s?(?:AM|PM)?$/i;
    return expr.test(time);
}