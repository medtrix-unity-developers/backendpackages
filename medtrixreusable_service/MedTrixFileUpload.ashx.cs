﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace MedTrixWebSiteService
{
    /// <summary>
    /// Summary description for MedTrixFileUpload
    /// </summary>
    public class MedTrixFileUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                //Fetch the Uploaded File.
                HttpPostedFile postedFile = context.Request.Files[0];
                string txtUName = context.Request.Form["txtUName"];
                string txtLName = context.Request.Form["txtLName"];
                string txtPhone = context.Request.Form["txtPhone"];
                string txtMail = context.Request.Form["txtMail"];
                string btnUpload = context.Request.Form["btnUpload"];
                string txtLinkedIn = context.Request.Form["txtLinkedIn"];
                string txtPersonalWebSite = context.Request.Form["txtPersonalWebSite"];
                string hidJobName = context.Request.Form["contentName"];

                //Set the Folder Path.
                string folderPath = context.Server.MapPath("~/Resumes/");

                //Set the File Name.
                string fileExtension = Path.GetExtension(postedFile.FileName);
                string fileName = Path.GetFileName(postedFile.FileName);

                //Save the File in Folder.
                string finalName = txtUName + " " + txtLName;
                string finalFinalName = txtUName + "_" + txtLName +"_"+ hidJobName;
                postedFile.SaveAs(folderPath + finalFinalName + fileExtension);

               
                bool flag = false;
                string htmlFileToEmailuser = string.Empty;
                string htmlFileToEmailHR = string.Empty;
                htmlFileToEmailuser = File.ReadAllText(string.Concat(ConfigurationManager.AppSettings["emailpath"], "resumeacknowlege.html"));
                if (!string.IsNullOrEmpty(htmlFileToEmailuser))
                {
                    htmlFileToEmailuser = htmlFileToEmailuser.Replace("[#name#]", finalName.Replace("_", " ")).Replace("[#jobtitle#]", hidJobName.Replace("_"," "));
                }
                if (EmailServices.SendAcknowlegdeEmail(txtMail, htmlFileToEmailuser, "Job Application - " + hidJobName.Replace("_", " ")))
                {
                    flag = true;
                    htmlFileToEmailHR = File.ReadAllText(string.Concat(ConfigurationManager.AppSettings["emailpath"], "hr_Email.html"));
                    if (!string.IsNullOrEmpty(htmlFileToEmailuser))
                    {
                        string url = "";
                        url = ConfigurationManager.AppSettings["resumeURL"] + finalFinalName + fileExtension;
                        htmlFileToEmailHR = htmlFileToEmailHR.Replace("[#alink#]", url).Replace("[#Job_title#]", hidJobName.Replace("_", " ")).Replace("[#Candidate_Name#]",finalName.Replace("_", " "));
                        EmailServices.Send_HrEmail(htmlFileToEmailHR, "Job Application - " + hidJobName.Replace("_", " "));
                    }
                }
                // return flag;
                // EmailServices.SendAcknowlegdeEmail(finalFinalName, txtMail,)
                //Send File details in a JSON Response.
                string json = new JavaScriptSerializer().Serialize(
                    new
                    {
                        name = fileName
                    });
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.ContentType = "text/json";
                context.Response.Write(json);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}