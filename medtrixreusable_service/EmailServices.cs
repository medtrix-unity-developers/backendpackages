﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;
using System.Web.Script.Serialization;
using System.Net;
using System.Collections.Generic;
namespace MedTrixWebSiteService
{
    public static class EmailServices
    {
        class Mail
        {
            public Dictionary<string, string> to = new Dictionary<string, string>();
            public string subject = "How are you?";
            public List<string> from = new List<string>();
            public string html = "<b>Hello world e-mail!!!</b>";
        }

        class Response
        {
            public string code = string.Empty;
        }
        public static bool Send_HrEmail(string message, string subject)
        {
            Mail m = new Mail();

            //m.to.Add(new Dictionary<string, object>());
            m.to[ConfigurationManager.AppSettings["hremail"].ToString()] = ConfigurationManager.AppSettings["hremail"].ToString();
            m.from.Add(ConfigurationManager.AppSettings["supportemail"].ToString());
            m.from.Add("MedTrix Healthcare");
            m.subject = subject;
            m.html = message;
            JavaScriptSerializer jserializer = new JavaScriptSerializer();

            string json = jserializer.Serialize(m);

            string base_url = "https://api.sendinblue.com/v2.0/";
            Stream stream = new MemoryStream();
            string url = base_url + "email";
            string content_type = "application/json";

            // Create request
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = content_type;
            request.Headers.Add("api-key", "M7BjyzQUEpFDg9rw");

            using (System.IO.Stream s = request.GetRequestStream())
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(s))
                    sw.Write(json);
            }

            try
            {
                HttpWebResponse response;
                response = request.GetResponse() as HttpWebResponse;
                // read the response stream and put it into a byte array
                stream = response.GetResponseStream() as Stream;
            }
            catch (System.Net.WebException ex)
            {
                // read the response stream If status code is other than 200 and put it into a byte array
                stream = ex.Response.GetResponseStream() as Stream;
            }

            byte[] buffer = new byte[32 * 1024];
            int nRead = 0;
            MemoryStream ms = new MemoryStream();
            do
            {
                nRead = stream.Read(buffer, 0, buffer.Length);
                ms.Write(buffer, 0, nRead);
            } while (nRead > 0);
            // convert read bytes into string
            ASCIIEncoding encoding = new ASCIIEncoding();
            String responseString = encoding.GetString(ms.ToArray());
            Response res = jserializer.Deserialize<Response>(responseString);
            return res.code.ToLower() == "success";
        }

        public static bool Send_ContactUS(string message, string subject)
        {
            Mail m = new Mail();

            //m.to.Add(new Dictionary<string, object>());
            m.to[ConfigurationManager.AppSettings["supportemail"].ToString()] = ConfigurationManager.AppSettings["supportemail"].ToString();
            m.from.Add(ConfigurationManager.AppSettings["supportemail"].ToString());
            m.from.Add("MedTrix Healthcare");
            m.subject = subject;
            m.html = message;
            JavaScriptSerializer jserializer = new JavaScriptSerializer();

            string json = jserializer.Serialize(m);

            string base_url = "https://api.sendinblue.com/v2.0/";
            Stream stream = new MemoryStream();
            string url = base_url + "email";
            string content_type = "application/json";

            // Create request
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = content_type;
            request.Headers.Add("api-key", "M7BjyzQUEpFDg9rw");

            using (System.IO.Stream s = request.GetRequestStream())
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(s))
                    sw.Write(json);
            }

            try
            {
                HttpWebResponse response;
                response = request.GetResponse() as HttpWebResponse;
                // read the response stream and put it into a byte array
                stream = response.GetResponseStream() as Stream;
            }
            catch (System.Net.WebException ex)
            {
                // read the response stream If status code is other than 200 and put it into a byte array
                stream = ex.Response.GetResponseStream() as Stream;
            }

            byte[] buffer = new byte[32 * 1024];
            int nRead = 0;
            MemoryStream ms = new MemoryStream();
            do
            {
                nRead = stream.Read(buffer, 0, buffer.Length);
                ms.Write(buffer, 0, nRead);
            } while (nRead > 0);
            // convert read bytes into string
            ASCIIEncoding encoding = new ASCIIEncoding();
            String responseString = encoding.GetString(ms.ToArray());
            Response res = jserializer.Deserialize<Response>(responseString);
            return res.code.ToLower() == "success";
        }

        public static bool SendAcknowlegdeEmail(string email, string message, string subject)
        {
            Mail m = new Mail();

            //m.to.Add(new Dictionary<string, object>());
            m.to[email] = email;
            m.from.Add(ConfigurationManager.AppSettings["supportemail"].ToString());
            m.from.Add("MedTrix Healthcare");
            m.subject = subject;
            m.html = message;
            JavaScriptSerializer jserializer = new JavaScriptSerializer();

            string json = jserializer.Serialize(m);

            string base_url = "https://api.sendinblue.com/v2.0/";
            Stream stream = new MemoryStream();
            string url = base_url + "email";
            string content_type = "application/json";

            // Create request
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = content_type;
            request.Headers.Add("api-key", "M7BjyzQUEpFDg9rw");

            using (System.IO.Stream s = request.GetRequestStream())
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(s))
                    sw.Write(json);
            }

            try
            {
                HttpWebResponse response;
                response = request.GetResponse() as HttpWebResponse;
                // read the response stream and put it into a byte array
                stream = response.GetResponseStream() as Stream;
            }
            catch (System.Net.WebException ex)
            {
                // read the response stream If status code is other than 200 and put it into a byte array
                stream = ex.Response.GetResponseStream() as Stream;
            }

            byte[] buffer = new byte[32 * 1024];
            int nRead = 0;
            MemoryStream ms = new MemoryStream();
            do
            {
                nRead = stream.Read(buffer, 0, buffer.Length);
                ms.Write(buffer, 0, nRead);
            } while (nRead > 0);
            // convert read bytes into string
            ASCIIEncoding encoding = new ASCIIEncoding();
            String responseString = encoding.GetString(ms.ToArray());
            Response res = jserializer.Deserialize<Response>(responseString);
            return res.code.ToLower() == "success";
        }

      
    }
}