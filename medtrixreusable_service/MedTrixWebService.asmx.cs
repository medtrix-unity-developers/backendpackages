﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace MedTrixWebSiteService
{
    /// <summary>
    /// Summary description for MedTrixWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MedTrixWebService : System.Web.Services.WebService
    {
        MedTrix medTrix = new MedTrix();


        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string InsertComments(string blogName, string userName, string userEmail, string comments, string frntvalue)
        {
            string insertData = "INSERT INTO Tbl_Comments (TypeName,Name,Email_Address,Comment,Submited) VALUES('" + blogName + "','" + userName + "','" + userEmail + "','" + comments + "','" + DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss") + "')";
            string insert = medTrix.SqlInsert(insertData);
            string updateComment = "UPDATE BlogData SET blogComment=" + frntvalue + " where TypeName='" + blogName + "'";
            string updateSql = medTrix.SqlInsert(updateComment);
            return "Hello World";
        }



        [WebMethod]
        public string InsertBlogVisited(string blogName)
        {
            string InsUpdateiRead = "", isql = "", condition = "", finalValue = "";
            SqlDataReader rs = null;
            SqlCommand cmd = null;

            using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString))
            {
                con1.Open();
                InsUpdateiRead = "SELECT * FROM dbo.BlogData WHERE TypeName = '" + blogName + "'";
                cmd = new SqlCommand(InsUpdateiRead, con1);
                rs = cmd.ExecuteReader();
                if (rs.Read())
                {
                    int vistied = 1;
                    if (rs["TypeName"] != System.DBNull.Value) // --- time left
                    {
                        //need to add Condition
                    }
                    if (rs["blog_visited"].ToString() == "")
                    {
                        vistied = 1;
                    }
                    else if (rs["blog_visited"].ToString() != "")
                    {
                        vistied = Convert.ToInt32(rs["blog_Visited"]) + 1;

                    }
                    isql = "UPDATE BlogData SET blog_visited=" + vistied + " WHERE TypeName = '" + blogName + "'";
                    condition = "Update";//Update
                }
                else
                {
                    isql = "INSERT INTO BlogData (TypeName,blog_visited,submitDated) VALUES('" + blogName + "','1','" + DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss") + "')";
                    condition = "Insert";//Insert
                }
                rs.Close();
                if (isql.Length > 0)
                {
                    try
                    {
                        cmd.CommandText = isql;
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex2)
                    {
                        condition = ex2.ToString();
                        //    strError = ex2.ToString();
                    }
                }
            }

            //if (condition == "Insert" || condition == "Update")
            //{
            //    string getalldata = "";
            //    SqlDataReader rs1 = null;
            //    SqlCommand cmd1 = null;

            //    using (SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["PTCDBCONTEXT"].ConnectionString))
            //    {
            //        con2.Open();
            //        getalldata = "SELECT SUM(Bookmark) AS ratings FROM UseriRead WHERE iread_no=" + iReadNo + " and Page_No=" + pageNo + " and Annotation_No=" + annotationNo + " and User_Id=" + userId + "";
            //        cmd1 = new SqlCommand(getalldata, con2);
            //        rs1 = cmd1.ExecuteReader();
            //        if (rs1.Read())
            //        {
            //            finalValue = rs1["ratings"].ToString();
            //        }

            //    }
            //}
            return finalValue;
        }

        [WebMethod]
        public string Like_Share_BlogVisited(string blogName, string type)
        {
            string InsUpdateiRead = "", isql = "", condition = "", finalValue = "";
            SqlDataReader rs = null;
            SqlCommand cmd = null;

            using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString))
            {
                con1.Open();
                string typeColumn = string.Empty;
                if (type == "1")
                {
                    typeColumn = "blogLikes";
                }
                else if (type == "3")
                {
                    typeColumn = "blogPrint";
                }
                else
                {
                    typeColumn = "blogShare";
                }
                InsUpdateiRead = "SELECT * FROM dbo.BlogData WHERE TypeName = '" + blogName + "'";
                cmd = new SqlCommand(InsUpdateiRead, con1);
                rs = cmd.ExecuteReader();
                if (rs.Read())
                {
                    int vistied = 1;
                    if (rs[typeColumn] == System.DBNull.Value) // --- time left
                    {
                        vistied = 1;
                        isql = "UPDATE BlogData SET " + typeColumn + "=" + vistied + " WHERE TypeName = '" + blogName + "'";
                    }
                    else if (rs[typeColumn] != "")
                    {
                        vistied = Convert.ToInt32(rs[typeColumn]) + 1;
                        isql = "UPDATE BlogData SET " + typeColumn + "=" + vistied + " WHERE TypeName = '" + blogName + "'";
                        condition = "Update";//Update
                    }
                    else
                    {
                        isql = "INSERT INTO BlogData (TypeName," + typeColumn + ",submitDated) VALUES('" + blogName + "','1','" + DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss") + "')";
                        condition = "Insert";//Insert
                    }
                    rs.Close();
                    if (isql.Length > 0)
                    {
                        try
                        {
                            cmd.CommandText = isql;
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex2)
                        {
                            condition = ex2.ToString();
                            //    strError = ex2.ToString();
                        }
                    }
                }


                return finalValue;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetBlogData(string blogName)
        {
            List<BlogData> blogList = new List<BlogData>();

            string cs = ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT * FROM BlogData WHERE TypeName='" + blogName + "'";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    BlogData blogData = new BlogData();

                    if (rdr["TypeName"].ToString() != "")
                    {
                        blogData.TypeName = rdr["TypeName"].ToString();
                    }
                    if (rdr["blogView"].ToString() != "")
                    {
                        blogData.blogView = Convert.ToInt32(rdr["blogView"]);
                    }
                    if (rdr["blogComment"].ToString() != "")
                    { blogData.blogComment = Convert.ToInt32(rdr["blogComment"]); }
                    if (rdr["blogShare"].ToString() != "")
                    { blogData.blogShare = Convert.ToInt32(rdr["blogShare"]); }
                    if (rdr["blogLikes"].ToString() != "")
                    { blogData.blogLikes = Convert.ToInt32(rdr["blogLikes"]); }
                    if (rdr["blogPrint"].ToString() != "")
                    { blogData.blogPrint = Convert.ToInt32(rdr["blogPrint"]); }
                    if (rdr["blog_visited"].ToString() != "")
                    { blogData.blog_visited = Convert.ToInt32(rdr["blog_visited"]); }
                    blogList.Add(blogData);
                }
            }

            return new JavaScriptSerializer().Serialize(blogList);


        }

        [WebMethod]
        public string InsertNewsComments(string newsName, string userName, string userEmail, string comments, string frntvalue)
        {
            string insertData = "INSERT INTO Tbl_NewsComments (TypeName,Name,Email_Address,Comment,Submited) VALUES('" + newsName + "','" + userName + "','" + userEmail + "','" + comments + "','" + DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss") + "')";
            string insert = medTrix.SqlInsert(insertData);
            string updateComment = "UPDATE NewsData SET NewsComment=" + frntvalue + " where TypeName='" + newsName + "'";
            string updateSql = medTrix.SqlInsert(updateComment);
            return "Hello World";
        }



        [WebMethod]
        public string InsertNewsVisited(string newsName)
        {
            string InsUpdateiRead = "", isql = "", condition = "", finalValue = "";
            SqlDataReader rs = null;
            SqlCommand cmd = null;

            using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString))
            {
                con1.Open();
                InsUpdateiRead = "SELECT * FROM dbo.NewsData WHERE TypeName = '" + newsName + "'";
                cmd = new SqlCommand(InsUpdateiRead, con1);
                rs = cmd.ExecuteReader();
                if (rs.Read())
                {
                    int vistied = 1;
                    if (rs["TypeName"] != System.DBNull.Value) // --- time left
                    {
                        //need to add Condition
                    }
                    if (rs["news_visited"] != "")
                    {
                        vistied = Convert.ToInt32(rs["News_Visited"]) + 1;
                    }
                    isql = "UPDATE NewsData SET News_visited=" + vistied + " WHERE TypeName = '" + newsName + "'";
                    condition = "Update";//Update
                }
                else
                {
                    isql = "INSERT INTO NewsData (TypeName,News_visited,submitDated) VALUES('" + newsName + "','1','" + DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss") + "')";
                    condition = "Insert";//Insert
                }
                rs.Close();
                if (isql.Length > 0)
                {
                    try
                    {
                        cmd.CommandText = isql;
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex2)
                    {
                        condition = ex2.ToString();
                        //    strError = ex2.ToString();
                    }
                }
            }

            //if (condition == "Insert" || condition == "Update")
            //{
            //    string getalldata = "";
            //    SqlDataReader rs1 = null;
            //    SqlCommand cmd1 = null;

            //    using (SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["PTCDBCONTEXT"].ConnectionString))
            //    {
            //        con2.Open();
            //        getalldata = "SELECT SUM(Bookmark) AS ratings FROM UseriRead WHERE iread_no=" + iReadNo + " and Page_No=" + pageNo + " and Annotation_No=" + annotationNo + " and User_Id=" + userId + "";
            //        cmd1 = new SqlCommand(getalldata, con2);
            //        rs1 = cmd1.ExecuteReader();
            //        if (rs1.Read())
            //        {
            //            finalValue = rs1["ratings"].ToString();
            //        }

            //    }
            //}
            return finalValue;
        }

        [WebMethod]
        public string Like_Share_NewsVisited(string newsName, string type)
        {
            string InsUpdateiRead = "", isql = "", condition = "", finalValue = "";
            SqlDataReader rs = null;
            SqlCommand cmd = null;

            using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString))
            {
                con1.Open();
                string typeColumn = string.Empty;
                if (type == "1")
                {
                    typeColumn = "NewsLikes";
                }
                else if (type == "3")
                {
                    typeColumn = "NewsPrint";
                }
                else
                {
                    typeColumn = "NewsShare";
                }
                InsUpdateiRead = "SELECT * FROM dbo.NewsData WHERE TypeName = '" + newsName + "'";
                cmd = new SqlCommand(InsUpdateiRead, con1);
                rs = cmd.ExecuteReader();
                if (rs.Read())
                {
                    int vistied = 1;
                    if (rs[typeColumn] == System.DBNull.Value) // --- time left
                    {
                        vistied = 1;
                        isql = "UPDATE NewsData SET " + typeColumn + "=" + vistied + " WHERE TypeName = '" + newsName + "'";
                    }
                    else if (rs[typeColumn] != "")
                    {
                        vistied = Convert.ToInt32(rs[typeColumn]) + 1;
                        isql = "UPDATE NewsData SET " + typeColumn + "=" + vistied + " WHERE TypeName = '" + newsName + "'";
                        condition = "Update";//Update
                    }
                    else
                    {
                        isql = "INSERT INTO NewsData (TypeName," + typeColumn + ",submitDated) VALUES('" + newsName + "','1','" + DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss") + "')";
                        condition = "Insert";//Insert
                    }
                    rs.Close();
                    if (isql.Length > 0)
                    {
                        try
                        {
                            cmd.CommandText = isql;
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex2)
                        {
                            condition = ex2.ToString();
                            //    strError = ex2.ToString();
                        }
                    }
                }


                return finalValue;
            }
        }

        [WebMethod]
        public bool ContactUs(string name, string phone, string email, string message)
        {



            bool flag = false;
            string htmlFileToEmailuser = string.Empty;
            string htmlFileToEmailHR = string.Empty;
            htmlFileToEmailuser = File.ReadAllText(string.Concat(ConfigurationManager.AppSettings["emailpath"], "contactus.html"));
            if (!string.IsNullOrEmpty(htmlFileToEmailuser))
            {
                htmlFileToEmailuser = htmlFileToEmailuser.Replace("[#name#]", name).Replace("[#phone#]",phone).Replace("[#email#]", email).Replace("[#msg#]", message);
            }
            if (EmailServices.Send_ContactUS(htmlFileToEmailuser, "MedTrix Corporate - Contact US"))
            {
                flag = true;
            }
            return flag;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetNewsData(string newsName)
        {
            List<NewsData> newsList = new List<NewsData>();

            string cs = ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT * FROM NewsData WHERE TypeName='" + newsName + "'";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    NewsData newsData = new NewsData();

                    if (rdr["TypeName"].ToString() != "")
                    {
                        newsData.TypeName = rdr["TypeName"].ToString();
                    }
                    if (rdr["NewsView"].ToString() != "")
                    {
                        newsData.NewsView = Convert.ToInt32(rdr["NewsView"]);
                    }
                    if (rdr["NewsComment"].ToString() != "")
                    { newsData.NewsComment = Convert.ToInt32(rdr["NewsComment"]); }
                    if (rdr["NewsShare"].ToString() != "")
                    { newsData.NewsShare = Convert.ToInt32(rdr["NewsShare"]); }
                    if (rdr["NewsLikes"].ToString() != "")
                    { newsData.NewsLikes = Convert.ToInt32(rdr["NewsLikes"]); }
                    if (rdr["NewsPrint"].ToString() != "")
                    { newsData.NewsPrint = Convert.ToInt32(rdr["NewsPrint"]); }
                    if (rdr["News_visited"].ToString() != "")
                    { newsData.News_visited = Convert.ToInt32(rdr["News_visited"]); }
                    newsList.Add(newsData);
                }
            }

            return new JavaScriptSerializer().Serialize(newsList);


        }

        public class BlogData
        {
            public string TypeName { get; set; }

            public string BlogName { get; set; }

            public string BlogDescription { get; set; }

            public string ReadMore { get; set; }

            public string BlogImage { get; set; }

            public string BlogDate { get; set; }

            public string BlogHref { get; set; }

            public Nullable<int> blogView { get; set; }
            public Nullable<int> blogComment { get; set; }
            public Nullable<int> blogShare { get; set; }
            public Nullable<int> blogLikes { get; set; }
            public Nullable<int> blogPrint { get; set; }
            public Nullable<int> blog_visited { get; set; }
        }

        public class BlogCommentsData
        {
            public string TypeName { get; set; }
            public string Name { get; set; }
            public string Comment { get; set; }
            public string Submitted { get; set; }
        }

        public class NewsCommentsData
        {
            public string TypeName { get; set; }
            public string Name { get; set; }
            public string Comment { get; set; }
            public string Submitted { get; set; }
        }

        public class NewsData
        {
            public string TypeName { get; set; }
            public Nullable<int> NewsView { get; set; }
            public Nullable<int> NewsComment { get; set; }
            public Nullable<int> NewsShare { get; set; }
            public Nullable<int> NewsLikes { get; set; }
            public Nullable<int> NewsPrint { get; set; }
            public Nullable<int> News_visited { get; set; }
        }

        [WebMethod]
        public string GetBlogsComments(string blogName)
        {
            List<BlogCommentsData> blogCommentList = new List<BlogCommentsData>();

            string cs = ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT TypeName,Name,Comment,CONVERT(varchar, Submited, 106) AS Submited from Tbl_Comments WHERE TypeName='" + blogName + "' order by Submited desc";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    BlogCommentsData blogData = new BlogCommentsData();

                    if (rdr["TypeName"].ToString() != "")
                    {
                        blogData.TypeName = rdr["TypeName"].ToString();
                    }
                    if (rdr["Name"].ToString() != "")
                    {
                        blogData.Name = rdr["Name"].ToString();
                    }
                    if (rdr["Comment"].ToString() != "")
                    { blogData.Comment = rdr["Comment"].ToString(); }
                    if (rdr["Submited"].ToString() != "")
                    { blogData.Submitted = rdr["Submited"].ToString(); }
                    blogCommentList.Add(blogData);
                }
            }

            return new JavaScriptSerializer().Serialize(blogCommentList);
        }

        [WebMethod]
        public string GetNewsComments(string newsName)
        {
            List<NewsCommentsData> newsCommentList = new List<NewsCommentsData>();

            string cs = ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "select TypeName,Name,Comment,convert(varchar, Submited, 106) as Submited from Tbl_NewsComments where TypeName='" + newsName + "' order by Submited desc";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    NewsCommentsData newsData = new NewsCommentsData();

                    if (rdr["TypeName"].ToString() != "")
                    {
                        newsData.TypeName = rdr["TypeName"].ToString();
                    }
                    if (rdr["Name"].ToString() != "")
                    {
                        newsData.Name = rdr["Name"].ToString();
                    }
                    if (rdr["Comment"].ToString() != "")
                    { newsData.Comment = rdr["Comment"].ToString(); }
                    if (rdr["Submited"].ToString() != "")
                    { newsData.Submitted = rdr["Submited"].ToString(); }
                    newsCommentList.Add(newsData);
                }
            }

            return new JavaScriptSerializer().Serialize(newsCommentList);
        }

        [WebMethod]
        public string getData(string value)
        {
            string test = "9586-202_10072";
            string lastFragment = test.Split('_').Last();
            return lastFragment;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetFullBlogData()
        {
            List<BlogData> blogList = new List<BlogData>();

            string cs = ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT * FROM BlogData order by CAST(blog_visited AS int) desc";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    BlogData blogData = new BlogData();

                    if (rdr["TypeName"].ToString() != "")
                    {
                        blogData.TypeName = rdr["TypeName"].ToString();
                    }
                    if (rdr["blogView"].ToString() != "")
                    {
                        blogData.blogView = Convert.ToInt32(rdr["blogView"]);
                    }
                    if (rdr["blogComment"].ToString() != "")
                    { blogData.blogComment = Convert.ToInt32(rdr["blogComment"]); }
                    if (rdr["blogShare"].ToString() != "")
                    { blogData.blogShare = Convert.ToInt32(rdr["blogShare"]); }
                    if (rdr["blogLikes"].ToString() != "")
                    { blogData.blogLikes = Convert.ToInt32(rdr["blogLikes"]); }
                    if (rdr["blogPrint"].ToString() != "")
                    { blogData.blogPrint = Convert.ToInt32(rdr["blogPrint"]); }
                    if (rdr["blog_visited"].ToString() != "")
                    { blogData.blog_visited = Convert.ToInt32(rdr["blog_visited"]); }
                    if (rdr["BlogName"].ToString() != "")
                    { blogData.BlogName = rdr["BlogName"].ToString(); }
                    if (rdr["BlogDescription"].ToString() != "")
                    { blogData.BlogDescription = rdr["BlogDescription"].ToString(); }
                    if (rdr["ReadMore"].ToString() != "")
                    { blogData.ReadMore = rdr["ReadMore"].ToString(); }
                    if (rdr["BlogImage"].ToString() != "")
                    { blogData.BlogImage = rdr["BlogImage"].ToString(); }
                    if (rdr["BlogDate"].ToString() != "")
                    { blogData.BlogDate = rdr["BlogDate"].ToString(); }
                    { blogData.BlogDate = rdr["BlogDate"].ToString(); }
                    blogList.Add(blogData);
                }
            }

            return new JavaScriptSerializer().Serialize(blogList);


        }



    }
}