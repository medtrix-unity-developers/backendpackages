﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedTrixWebSiteService
{
    public class MedTrix
    {
        private String ConStr = String.Empty;
        public string strError = "";
        private readonly String StrCon;
        public MedTrix()
        {

            StrCon = ConfigurationManager.ConnectionStrings["MedTrixDB"].ConnectionString; // mysql
        }
        public string SqlInsert(string SqlI)
        {

            string err = null;
            string ext = null;
            SqlCommand sqlcmd = null;
            using (SqlConnection Conn = new SqlConnection(StrCon))
            {
                try
                {
                    Conn.Open();
                    sqlcmd = new SqlCommand(SqlI, Conn);
                    sqlcmd.Connection = Conn;
                    sqlcmd.ExecuteNonQuery();
                    ext = "Y";
                }
                catch (Exception ex)
                {
                    ext = ex.ToString();
                    err = ex.ToString();
                }
                finally
                {
                    sqlcmd.Dispose();
                    Conn.Close();
                }
                return ext;
            }
        }
    }
}